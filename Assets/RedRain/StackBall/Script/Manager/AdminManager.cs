using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enumeration;
using UnityEngine.Events;
public class AdminManager : MonoBehaviour
{

    [Tooltip("When The Game Start")]
    public UnityEvent m_StartEvent;


    [Tooltip("When The Game End")]
    public UnityEvent m_EndEvent;

    public float m_Timer;
    public DecreaseType m_DecreaseType;
    public float m_PointsDecreasePerFruit;
    public float m_PointsIncreasePerFruit;
    public float[] m_ScoresTarget;
    public bool m_IsLocal;

    //Send Score To Backend Function
    public void SendScoreToBackEnd(float p_Score) {

    }

    //Send HighScore To Backend Function
    public float GetHighScoreFromBackEnd() {
        return 0f;
    }
}
