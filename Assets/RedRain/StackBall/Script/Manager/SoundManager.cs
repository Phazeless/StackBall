using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enumeration;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    public AudioSource m_BGMSource;
    public AudioSource m_SoundEffectCutSource;
    public AudioSource m_SoundEffectSource;

    public AudioClip m_BGM;
    public AudioClip m_BGMFever;
    public AudioClip[] m_SFX;

    SFXType m_CurrentSfxType;
    public float m_StartObjectPitch;
    public float m_StartFeverPitch;
    private void Awake() {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayBGM(bool isFever) {
        if (isFever) {
            m_BGMSource.clip = m_BGMFever;
        } else {
            m_BGMSource.clip = m_BGM;
        }
        m_BGMSource.Play();
    } 

    public void PlaySoundEffect(SFXType p_AudioType) {
        m_SoundEffectSource.PlayOneShot(m_SFX[(int)p_AudioType] );
    }

    public void PlaySoundEffectNormal(SFXType p_AudioType, float p_Pitch) {
        if(m_CurrentSfxType != p_AudioType) {
            m_CurrentSfxType = p_AudioType;
            if (m_CurrentSfxType == SFXType.Fever)
                m_SoundEffectCutSource.pitch = m_StartFeverPitch;
            else m_SoundEffectCutSource.pitch = m_StartObjectPitch;
        } else {
            m_SoundEffectCutSource.pitch += p_Pitch;
        }
        m_SoundEffectCutSource.PlayOneShot(m_SFX[(int)p_AudioType]);
    }

    public void Mute() {
        if (!m_BGMSource.mute) {
            m_BGMSource.mute = true;
            m_SoundEffectSource.mute = true;
        } else {
            m_BGMSource.mute = false;
            m_SoundEffectSource.mute = false;
        }    
    }
}
