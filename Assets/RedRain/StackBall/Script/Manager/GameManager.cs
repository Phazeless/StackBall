using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enumeration;
using TMPro;
using UnityEngine.UI;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [HideInInspector]
    public float m_Timer;

    [HideInInspector]
    public float[] m_TargetScore;

    [Header("Score")]
    public float m_Score;
    public float m_HighScore;

    [Header("Game State")]
    public GameState m_GameState;

    [Header("Manager")]
    public AdminManager m_AdminManager;

    [Header("Sickle GameObject")]
    public Sickle_GameObject m_Sickle;

    [Header("Sickle Default Position")]
    public Transform m_Position;

    [Header("UI")]
    public GameObject m_StartUI;
    public GameObject m_IngameUI;
    public GameObject m_EndUI;
    public GameObject[] m_StarGameObject;

    [Header("InGameText")]
    public TextMeshProUGUI m_TimerText;
    public TextMeshProUGUI m_ScoreText;

    [Header("ResultText")]
    public TextMeshProUGUI m_ResultScoreText;
    public TextMeshProUGUI m_HighScoreText;

    [Header("HighScore Animation")]
    public Animator m_HighScoreAnimation;

    [Header("Bar UI")]
    public RectTransform m_StarParent;
    public RectTransform[] m_StarsPosition;
    public GameObject[] m_StarFilledGameObject;
    public TextMeshProUGUI[] m_ScoreBarGameObject;
    public Image m_BarFill;

    Vector3 m_StarPosition;
    float m_currentTimer;

    public float m_CurrentTimer {
        set {
            m_currentTimer = value;
            m_TimerText.text = value.ToString("F0");
        }
        get {
            return m_currentTimer;
        }
    }

    private void Awake() {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        m_AdminManager.m_StartEvent?.Invoke();
        m_TargetScore = m_AdminManager.m_ScoresTarget;
        m_Timer = m_AdminManager.m_Timer;
        UpdatePosition();
        FillBar(m_Score);
    }

    // Update is called once per frame
    void Update()
    {
        if (m_GameState != GameState.Game) return;
        FillBar(m_Score);
        m_ScoreText.text = "" + m_Score.ToString("F0");
        m_CurrentTimer -= Time.deltaTime;
        if(m_CurrentTimer <= 0) {
            EndGame();
        }

    }

    public void StartGame() {
        SoundManager.instance.PlaySoundEffect(SFXType.Button);
        m_Score = 0;
       
        m_HighScore = GetHighScore();
        
        m_CurrentTimer = m_Timer;
        
        m_Sickle.PrepareTheBall();
        
        m_IngameUI.SetActive(true);
        m_StartUI.SetActive(false);
        
        m_GameState = GameState.Game;
    }

    public void EndGame() {
        m_GameState = GameState.End;
        m_IngameUI.SetActive(false);
        m_EndUI.SetActive(true);

        SetStars(m_Score);
        m_ResultScoreText.text = m_Score.ToString("F0");
        if (IsHighScore()) m_HighScoreAnimation.Play("Highscore_Spawn");
        m_HighScoreText.text = m_HighScore.ToString("F0");
        if(!m_AdminManager.m_IsLocal) m_AdminManager.SendScoreToBackEnd(m_Score);
        m_AdminManager.m_EndEvent?.Invoke();
        m_Sickle.m_Clicking = false;
        m_Sickle.transform.position = m_Position.position;
    }

   
    public void MainMenu() {
        m_CurrentTimer = m_Timer;
        SoundManager.instance.PlaySoundEffect(SFXType.Button);

        m_EndUI.SetActive(false);
        m_StartUI.SetActive(true);
        
        m_GameState = GameState.Start;
    }

    public void Quit() {
        SoundManager.instance.PlaySoundEffect(SFXType.Button);
    }

    bool IsHighScore() {
        if (m_HighScore < m_Score) {
            PlayerPrefs.SetFloat("HighScore", m_Score);
            GetHighScore();
            return true;
        }

        return false;
    }

    float GetHighScore() {
        if (m_AdminManager.m_IsLocal) {
            return PlayerPrefs.GetFloat("HighScore", 0);
        } else {
            return m_AdminManager.GetHighScoreFromBackEnd();
        }
    
    }

    public void UpdateScore(float p_Value) {
        m_Score += p_Value;
        if (m_Score <= 0) {
            m_Score = 0;
        }
    }
    
    public void MissedScore(float p_Value) {
       
        if(m_AdminManager.m_DecreaseType == DecreaseType.Score) {
            UpdateScore(p_Value);
        } else {
            m_currentTimer -= p_Value;
        }
       
    }

    public void SetStars(float p_Score) {
        for(int i = 0; i < m_StarGameObject.Length; i++) {
            m_StarGameObject[i].SetActive(false);
            if (p_Score >= m_TargetScore[i]) {
                m_StarGameObject[i].SetActive(true);
            }
        }
    }

    public void UpdatePosition() {
        for(int i = 0; i < m_StarsPosition.Length; i++) {
            m_StarPosition = m_StarsPosition[i].anchoredPosition;
            m_StarPosition.y = m_TargetScore[i] / m_TargetScore[m_TargetScore.Length - 1] * m_StarParent.rect.height;
            m_StarsPosition[i].anchoredPosition = m_StarPosition;
        }
        UpdateText();
    }

    public void UpdateText() {
        for (int i = 0; i < m_ScoreBarGameObject.Length; i++) {
            m_ScoreBarGameObject[i].text = m_TargetScore[i].ToString("F0");
        }
    }

    public void FillBar(float p_Score) {
        for (int i = 0; i < m_StarGameObject.Length; i++) {            
            m_StarFilledGameObject[i].SetActive(false);
            if (p_Score >= m_TargetScore[i]) {
                m_StarFilledGameObject[i].gameObject.SetActive(true);
            }
        }

        m_BarFill.fillAmount = m_Score / m_TargetScore[m_TargetScore.Length - 1];

    }
}
