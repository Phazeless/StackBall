using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enumeration {
    public enum FruitType {
        Grapes = 0,
        Lemon,
        Carrot
    }

    public enum GameState {
        Start,
        Game,
        End
    }

    public enum PlatformType
    {
        ThreePattern = 0,
        FourPattern,
        FivePattern,
        SixPattern,

    }

    public enum SFXType {
        Button = 0,
        Star,
        Fever,
        Right,
        Wrong,
    }

    public enum DecreaseType {
        Timer,
        Score
    }
}