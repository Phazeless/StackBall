using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Particle_SystemCallback : MonoBehaviour
{
    public UnityEvent m_Event;
    public void OnParticleSystemStopped() {
        m_Event?.Invoke();
    }
}
