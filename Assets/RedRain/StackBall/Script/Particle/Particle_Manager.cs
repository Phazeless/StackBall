 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enumeration;
public class Particle_Manager : MonoBehaviour
{
    public static Particle_Manager instance;

    public List<Particle_GameObject> m_ListParticles;
    
    public Particle_GameObject[] m_ParticlePrefabs;

    public Transform m_ParentTransform;

    int m_Index;
    private void Awake() {
        instance = this;
    }

    public void SpawnParticle(FruitType p_Type, Vector3 p_Position) {
        m_Index = GetIndex(p_Type);

        if (m_Index < 0) {
            m_ListParticles.Add(Instantiate(m_ParticlePrefabs[(int)p_Type]));
           
            m_Index = m_ListParticles.Count - 1;
        }

        m_ListParticles[m_Index].transform.SetParent(m_ParentTransform);
        m_ListParticles[m_Index].transform.position = p_Position;
        m_ListParticles[m_Index].gameObject.SetActive(true);

    }

    int GetIndex(FruitType p_Type) {
        for(int i = 0; i < m_ListParticles.Count; i++) {
            if(!m_ListParticles[i].gameObject.activeSelf && m_ListParticles[i].m_FruitType == p_Type) return i; 
        }

        return -1;
    }

}
