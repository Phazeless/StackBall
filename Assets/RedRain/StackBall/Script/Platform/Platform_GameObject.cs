using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Platform_GameObject : MonoBehaviour
{
    public List<PlatformChild_GameObject> m_Child;
    public List<Transform> m_ParticlePosition;

    public void Update() {
        transform.Rotate(0, -50f * Time.deltaTime, 0);
    }

}
