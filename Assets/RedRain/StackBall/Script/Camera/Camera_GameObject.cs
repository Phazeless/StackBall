using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_GameObject : MonoBehaviour
{
    public Sickle_GameObject m_Target;
    public float m_OffSet;
    public float m_SmoothTime;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.y <= Log_Manager.instance.m_CurrentSpawnObject.transform.position.y) {
            Log_Manager.instance.Spawn();
        }   
    }

    private void LateUpdate() {
        FollowTarget(LevelGenerator.instance.transform.GetChild(0).transform);
    }

    private void FollowTarget(Transform p_Target) {
        transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, p_Target.position.y + m_OffSet, m_SmoothTime * Time.deltaTime), transform.position.z);
    }

}
