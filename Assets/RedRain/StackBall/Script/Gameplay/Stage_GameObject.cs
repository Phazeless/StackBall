using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage_GameObject : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() {
        FollowTarget(LevelGenerator.instance.transform.GetChild(LevelGenerator.instance.transform.childCount - 1).transform);

    }
    private void FollowTarget(Transform p_Target) {
        transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, p_Target.position.y, 100000f * Time.deltaTime), transform.position.z);
    }

}
