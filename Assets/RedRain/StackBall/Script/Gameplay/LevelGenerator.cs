using Enumeration;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public static LevelGenerator instance;

    [SerializeField]
    private PlatformVariant_GameObject[] m_Platforms;

    [SerializeField]
    private PlatformType m_PlatformType;

    [SerializeField]
    private float m_AngleStep;

    [SerializeField]
    private int m_PlatformAmount;

    [SerializeField]
    private float m_PlatformHeight;

    public int m_PlatformPattern;
    int m_CurrentPlatformPattern;

    [SerializeField]
    int m_CurrentSpawnIndex;

    [SerializeField]
    private List<PlatformVariant_GameObject> m_PlatformList;

    [SerializeField]
    private Transform m_PlatformUnusedParent;

    PlatformVariant_GameObject m_SpawnedPlatform;

    int m_PlatformRandomize;
    int m_SpawnIndex;
    int m_PlatformVariant;
    private void Awake() {
        instance = this;
    }

    [ContextMenu("GenerateLevel")]
    public void GenerateLevel() {
        for (int i = 0; i < m_PlatformAmount; i++) {
            SpawnNextObject();
        }
    }

    [ContextMenu("Clean")]
    public void Clean() {
        for (int i = 0; i < m_PlatformList.Count; i++) {
            m_PlatformList[i].gameObject.SetActive(false);
        }
    }


    [ContextMenu("Destroy")]
    public void Destroy() {
        for (int i = 0; i < m_PlatformList.Count; i++) {
            DestroyImmediate(m_PlatformList[i].gameObject);
        }
        m_PlatformList.Clear();
        m_CurrentSpawnIndex = 0;
    }

    public void GeneratePlatform() {
        if (m_CurrentPlatformPattern >= m_PlatformPattern) {
            if(m_CurrentSpawnIndex < 60) {
                m_PlatformRandomize = Random.Range(0, 100);
                if(m_PlatformRandomize < 50) {
                    m_PlatformType = PlatformType.ThreePattern;
                } else {
                    m_PlatformType = PlatformType.FourPattern;
                }
            } else if(m_CurrentSpawnIndex >= 60 && m_CurrentSpawnIndex < 120) {
                m_PlatformRandomize = Random.Range(0, 100);
                if (m_PlatformRandomize < 20) {
                    m_PlatformType = PlatformType.ThreePattern;
                } else if (m_PlatformRandomize < 70) {
                    m_PlatformType = PlatformType.FourPattern;
                } else {
                    m_PlatformType = PlatformType.FivePattern;
                }
            } else {
                m_PlatformRandomize = Random.Range(0, 100);
                if (m_PlatformRandomize < 10) {
                    m_PlatformType = PlatformType.ThreePattern;
                } else if (m_PlatformRandomize < 40) {
                    m_PlatformType = PlatformType.FourPattern;
                } else if (m_PlatformRandomize < 75) {
                    m_PlatformType = PlatformType.FivePattern;
                } else {
                    m_PlatformType = PlatformType.SixPattern;
                }
            }
           
            m_CurrentPlatformPattern = 0;
            GenerateChildPlatform((int)m_PlatformType,true);

        } else {
            m_CurrentPlatformPattern++;
            GenerateChildPlatform((int)m_PlatformType, false);
        }
       
        
    }

    public void GenerateChildPlatform(int p_PlatformType, bool p_IsTrue) {
        if (p_IsTrue) {
            m_PlatformVariant = Random.Range(0, m_Platforms[p_PlatformType].transform.childCount - 1);
        }
       

        SpawnObject(m_Platforms[p_PlatformType], Vector3.up * -m_PlatformHeight * m_CurrentSpawnIndex, Quaternion.Euler(0, m_AngleStep * m_CurrentSpawnIndex, 0), transform, m_PlatformVariant);
    }

    public void SpawnObject(PlatformVariant_GameObject p_Platform, Vector3 p_Position, Quaternion p_Rotation, Transform p_Parent, int p_PlatformVariant) {
        m_SpawnIndex = GetIndex(p_Platform.m_PlatformType);
        if (m_SpawnIndex < 0) {
           
            m_PlatformList.Add(Instantiate(p_Platform, p_Position, p_Rotation));
            m_SpawnIndex = m_PlatformList.Count - 1;
        }

        m_SpawnedPlatform = m_PlatformList[m_SpawnIndex];

        for(int i = 0; i < m_SpawnedPlatform.transform.childCount; i++) {
            if (i == p_PlatformVariant) {
                m_SpawnedPlatform.transform.GetChild(i).gameObject.SetActive(true);
                m_SpawnedPlatform.transform.GetChild(i).transform.rotation = Quaternion.identity;
            } else m_SpawnedPlatform.transform.GetChild(i).gameObject.SetActive(false);
        }

        m_SpawnedPlatform.transform.SetParent(p_Parent);
        m_SpawnedPlatform.transform.position = p_Position;
        m_SpawnedPlatform.transform.rotation = p_Rotation;
        m_SpawnedPlatform.transform.SetAsLastSibling();
        m_SpawnedPlatform.gameObject.SetActive(true);
    }

    public void Remove(GameObject p_Object) {
        p_Object.transform.SetParent(m_PlatformUnusedParent);
        p_Object.transform.SetAsLastSibling();
        p_Object.SetActive(false);
    }

    public void SpawnNextObject() {
        GeneratePlatform();
        m_CurrentSpawnIndex++;
    }

    int GetIndex(PlatformType p_PlatformType) {
        for(int i = 0; i < m_PlatformList.Count; i++) {
            if (!m_PlatformList[i].gameObject.activeSelf && p_PlatformType == m_PlatformList[i].m_PlatformType) return i;
        }

        return -1;
    }

   
}
