using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enumeration;
using EZCameraShake;
public class Sickle_GameObject : MonoBehaviour
{

    public FruitType m_FruitType;
    public Rigidbody m_Rb;
    public float m_JumpSpeed;
    
    public Transform m_IdlePosition;
    public Transform m_TappedPosition;
    
    public bool m_Clicking = false;
    
    public Animator m_Animator;
    
    public bool m_Tapped;
    public Vector3 m_Offset;
    
    public GameObject[] m_ListBalls;
    public RaycastHit[] m_Hits;

    public GameObject m_Vignette;
    public float m_VignetteDuration;

    public int m_Combo;
    public float m_EachComboDuration;
    float m_CurrentComboDuration;

    public float m_FeverDuration;
    public GameObject m_FeverObject;

    int m_CurrentCombo;
    bool m_Fever;

    private void Update() {
        ComboCountdown();
        if (Input.GetMouseButtonDown(0)) m_Clicking = true;
        if (Input.GetMouseButtonUp(0)) m_Clicking = false;
        

        if (!m_Clicking || !GameManager.instance.m_GameState.Equals(GameState.Game)) {
            if(!m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Sickle_wrong")) m_Animator.Play("Sickle");
            m_Rb.useGravity = false;
            transform.position = m_IdlePosition.position;
            m_Tapped = false;
            m_Rb.velocity = Vector3.zero;
            return;
        }
        else {
            m_Rb.useGravity = true;
          
            if (!m_Tapped) {
                transform.position = m_TappedPosition.position;
                m_Tapped = true;
            }

            m_Hits = Physics.RaycastAll(transform.position + m_Offset, transform.TransformDirection(Vector3.down) * 0.1f, 0.1f, 1);
            foreach (RaycastHit m_Hit in m_Hits) {
                if (m_Hit.transform.gameObject.tag.Equals("Fruit")) {
                    m_Animator.Play("Sickle_tapped");
                    if (m_Hit.transform.GetComponent<PlatformChild_GameObject>().m_FruitType == m_FruitType || m_Fever) {
                        LevelGenerator.instance.Remove(m_Hit.transform.parent.parent.gameObject);
                        if (!m_Fever) SoundManager.instance.PlaySoundEffectNormal(SFXType.Right, 0.05f);
                        else SoundManager.instance.PlaySoundEffectNormal (SFXType.Fever, 0.05f);
                        for(int i = 0; i < m_Hit.transform.parent.GetComponent<Platform_GameObject>().m_Child.Count; i++) {
                            ParticleBreak_Manager.instance.SpawnParticle(m_Hit.transform.parent.GetComponent<Platform_GameObject>().m_Child[i].m_FruitType,
                                m_Hit.transform.parent.GetComponent<Platform_GameObject>().m_ParticlePosition[i].position,
                                m_Hit.transform.parent.GetComponent<Platform_GameObject>().m_ParticlePosition[i].rotation);
                        }
                       
                        Particle_Manager.instance.SpawnParticle(m_FruitType, transform.position + m_Offset);
                        m_Rb.velocity = -Vector3.up * m_JumpSpeed;
                        Fever();
                        GameManager.instance.UpdateScore(GameManager.instance.m_AdminManager.m_PointsIncreasePerFruit);
                        LevelGenerator.instance.SpawnNextObject();
                    }
                    else {
                        StartCoroutine(Missed());
                        SoundManager.instance.PlaySoundEffect(SFXType.Wrong);
                        SoundManager.instance.m_SoundEffectCutSource.pitch = 0.7f;
                        CameraShaker.Instance.ShakeOnce(4f,4f, .1f, .2f);
                        m_CurrentCombo = 0;
                        m_Animator.Play("Sickle_wrong");
                        GameManager.instance.MissedScore(-GameManager.instance.m_AdminManager.m_PointsDecreasePerFruit);
                    }

                    return;
                }
            }
        }
    }

    public void PrepareTheBall() {
        for (int i = 0; i < m_ListBalls.Length; i++) m_ListBalls[i].gameObject.SetActive(false);
        m_FruitType = Random.Range(0, 100) <= 30 ? FruitType.Carrot : Random.Range(0, 100) > 50 ? FruitType.Lemon : FruitType.Grapes;
        m_ListBalls[(int)m_FruitType].gameObject.SetActive(true);
    }

    public void Fever() {
        m_CurrentCombo++;
        m_CurrentComboDuration = m_EachComboDuration;
        if(m_CurrentCombo >= m_Combo && !m_Fever) {
            m_Fever = true;
            StartCoroutine(FeverCountdown());
        }
    }

    public void ComboCountdown() {
        m_CurrentComboDuration -= Time.deltaTime;
        if(m_CurrentComboDuration <= 0) {
            if(m_CurrentCombo <= 0) {
                m_CurrentCombo = 0;
            } else {
                m_CurrentCombo--;
            }
            m_CurrentComboDuration = m_EachComboDuration;
        }
    }
    
    public IEnumerator FeverCountdown() {
        SoundManager.instance.PlayBGM(true);
        m_FeverObject.SetActive(true);
        yield return new WaitForSeconds(m_FeverDuration);
        m_Fever = false;

        SoundManager.instance.PlayBGM(false);
        m_CurrentCombo = 0;
        m_FeverObject.SetActive(false);

    }

    public IEnumerator Missed() {
        GameManager.instance.m_GameState = GameState.End;
        m_Vignette.SetActive(true);
        yield return new WaitForSeconds(m_VignetteDuration);
        GameManager.instance.m_GameState = GameState.Game;
        m_Vignette.SetActive(false);
    }
}