using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Log_Manager : MonoBehaviour
{
    public static Log_Manager instance;

    public Log_GameObject m_LogPrefab;
    public List<Log_GameObject> m_ListLogs;

    [SerializeField]
    int m_CurrentSpawnIndex;

    [SerializeField]
    Vector3 m_LogPosition;

    [SerializeField]
    float[] m_ListRotations;

    public Transform m_CurrentSpawnObject;

    Log_GameObject m_SpawnedLog;
    int m_SpawnIndex;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Spawn() {
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(0).SetAsLastSibling();
        m_CurrentSpawnObject = transform.GetChild(2);

        m_SpawnIndex = GetIndex();
        
        if(m_SpawnIndex < 0) {
            m_ListLogs.Add(m_LogPrefab);
            m_SpawnIndex = m_ListLogs.Count - 1;
        }

        m_SpawnedLog = m_ListLogs[m_SpawnIndex];
        m_LogPosition.y += m_CurrentSpawnIndex;
        m_SpawnedLog.transform.position = m_LogPosition;
        m_SpawnedLog.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, m_ListRotations[Random.Range(0, m_ListRotations.Length - 1)], transform.rotation.z));
        m_SpawnedLog.gameObject.SetActive(true);
    }

    int GetIndex() {
        for(int i = 0; i < m_ListLogs.Count; i++) {
            if (!m_ListLogs[i].gameObject.activeSelf) return i;
        }

        return -1;
    }

}
